<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Online Groceries Store</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body style="background-color: white;">
	
		<div class="container">
			<br> <br>
			<center>
				<u><h2>Groceries</h2></u>
			</center>
			<br>
			<table class="table" border='0px'>
				<thead>
					<tr>
						
						<th>ProductName</th>
						<th>Size</th>
						<th>UnitPrice</th>

					</tr>
				</thead>

				<tbody>
					<c:forEach items="${gList}" var="glist">

						<tr>
							
							<td>${glist.name}</td>
							<td>${glist.size}</td>
							<td>${glist.price}</td>

						</tr>

					</c:forEach>

				</tbody>
			</table>
			<br> <br>
			<center>
				<a href="addform">Add Items</a> 
				<a href="editform">Edit Items</a>
			</center>
		</div>
	
</body>
</html>