package trg.talentsprint.sample;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class GroceriesDao {

	public int save(Groceries g) {
		int status = 0;
		PreparedStatement ps = null;
		try {
			Connection con = DaoUtility.getConnection();
			ps = con.prepareStatement("insert into groceries(name,size,price) values (?,?,?)",
					PreparedStatement.RETURN_GENERATED_KEYS);
			ps.setString(1, g.getName());
			ps.setString(2, g.getSize());
			ps.setInt(3, g.getPrice());
			status = ps.executeUpdate();

			con.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return status;
	}

	public int update(Groceries g) {
		int status = 0;
		PreparedStatement ps = null;
		try {
			Connection con = DaoUtility.getConnection();
			ps = con.prepareStatement("update groceries set name=?,size=?,price=? where id=?");
			ps.setString(1, g.getName());
			ps.setString(2, g.getSize());
			ps.setInt(3, g.getPrice());
			ps.setInt(4, g.getId());
			status = ps.executeUpdate();

			con.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return status;
	}

	public int delete(int id) {
		int status = 0;
		PreparedStatement ps = null;
		try {
			Connection con = DaoUtility.getConnection();
			ps = con.prepareStatement("delete from groceries where id=?");
			ps.setInt(1, id);
			status = ps.executeUpdate();

			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return status;
	}

	public Groceries getGroceriesId(int id) {
		Groceries g = new Groceries();
		try {
			Connection con = DaoUtility.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from groceries where id=?");
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				g.setId(rs.getInt(1));
				g.setName(rs.getString(2));
				g.setSize(rs.getString(3));
				g.setPrice(rs.getInt(4));
			}

			con.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return g;
	}

	public List<Groceries> getGroceriesList() throws ClassNotFoundException, SQLException {
		List<Groceries> list = new ArrayList<Groceries>();
		Connection con = DaoUtility.getConnection();
		Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery("select * from groceries");
		Groceries g;
		while (rs.next()) {
			g = new Groceries();
			g.setId(rs.getInt(1));
			g.setName(rs.getString(2));
			g.setSize(rs.getString(3));
			g.setPrice(rs.getInt(4));
			list.add(g);

		}

		return list;

	}
}
