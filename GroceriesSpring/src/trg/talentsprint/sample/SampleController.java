package trg.talentsprint.sample;

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.servlet.ModelAndView;

@Controller
public class SampleController {

	private static final Logger logger = Logger.getLogger(SampleController.class);

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView printgrocerieslist(ModelAndView model) throws ClassNotFoundException, SQLException {

		if (logger.isDebugEnabled())
		{
			logger.debug("getWelcome is executed!");
		}

		GroceriesDao g1 = new GroceriesDao();

		List<Groceries> list = g1.getGroceriesList();
		model.addObject("gList", list);
		model.setViewName("home");

		return model;

	}

	@RequestMapping(value = "/addform")
	public String showform(Model m) {
		m.addAttribute("command", new Groceries());
		return "addform";
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String save(@ModelAttribute("gList") Groceries g) {
		GroceriesDao g1 = new GroceriesDao();
		g1.save(g);
		System.out.println("sample controller");
		return "redirect:/home";
	}

	@RequestMapping("/home")
	public String viewemp(Model m) throws ClassNotFoundException, SQLException {
		GroceriesDao g1 = new GroceriesDao();
		List<Groceries> list = g1.getGroceriesList();
		m.addAttribute("gList", list);
		return "home";
	}

	@RequestMapping(value = "/editform")
	public String showeditform(Model m) throws ClassNotFoundException, SQLException {
		GroceriesDao g1 = new GroceriesDao();
		List<Groceries> list = g1.getGroceriesList();
		m.addAttribute("gList", list);
		// m.addAttribute("command", new Groceries());
		return "editform";
	}

	@RequestMapping(value = "update/{id}")
	public String showupdateform(@PathVariable int id, Model m) {
		GroceriesDao g1 = new GroceriesDao();
		Groceries g = g1.getGroceriesId(id);
		m.addAttribute("command", g);
		return "updateform";
	}

	@RequestMapping(value = "/editsave", method = RequestMethod.POST)
	public String editsave(@ModelAttribute("gList") Groceries g) {
		GroceriesDao g1 = new GroceriesDao();
		g1.update(g);
		return "redirect:/home";
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public String delete(@PathVariable int id) {
		GroceriesDao g1 = new GroceriesDao();
		g1.delete(id);
		return "redirect:/home";
	}
}
